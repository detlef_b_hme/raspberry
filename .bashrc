alias ll='ls -al'
#export PS1='\u@\h> '
export LS_OPTIONS='--color=auto'
export HISTCONTROL=ignoredups:erasedups
eval "`dircolors`"
alias ls='ls $LS_OPTIONS'
export HISTTIMEFORMAT="%F %T "