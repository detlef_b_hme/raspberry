#!/bin/bash
#set -eux


token=$(cat ~/.token)


curl -q -H "Accept: application/json" -X PUT --data '{"on":false}' http://192.168.2.9/api/$token/lights/6/state |jq .
curl -q -H "Accept: application/json" -X PUT --data '{"on":false}' http://192.168.2.9/api/$token/lights/7/state |jq .
curl -q -H "Accept: application/json" -X PUT --data '{"on":false}' http://192.168.2.9/api/$token/lights/8/state |jq .
curl -q -H "Accept: application/json" -X PUT --data '{"on":false}' http://192.168.2.9/api/$token/lights/9/state |jq .
