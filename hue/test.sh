#!/bin/sh
set -eux

token=$(cat ~/.token)

a=$(curl -s http://192.168.2.9/api/$token/lights)
echo $a | grep -o -P  "\"name.*?[,]" > 0
echo $a | egrep -o  "\"[0-9]{1,2}\":{1}" >1
paste 1 0 | sed 's/\([":,]\|name\)//g'
rm 1 0
