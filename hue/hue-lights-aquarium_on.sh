#!/bin/bash
#set -eux

currenttime=$(date |awk  ' { print $4}'|awk -F : ' { print $1":"$2 }')
sunsettime=$(cat /home/detlef/tmp/sunrise_sunset |jq -r '.sys|.sunset')
sunsettime1=$(date -d "1970-01-01 ${sunsettime} sec GMT" |awk ' { print $4 }'|awk -F : ' { print $1":"$2 }')

token=$(cat ~/.token)
huex=$(shuf -i2000-55000 -n1)
brix=$(shuf -i100-200 -n1)
satx=$(shuf -i100-254 -n1)
tranx=5000

echo $currenttime
echo $sunsettime1
echo $sunsettime
echo $?


curl -q -H "Accept: application/json" -X PUT --data '{"on":true}' http://192.168.2.9/api/$token/lights/13/state |jq .
curl -q -H "Accept: application/json" -X PUT --data '{"on":true}' http://192.168.2.9/api/$token/lights/14/state |jq .
curl -q -H "Accept: application/json" -X PUT --data '{"on":true}' http://192.168.2.9/api/$token/lights/15/state |jq .
curl -q -H "Accept: application/json" -X PUT --data '{"on":true}' http://192.168.2.9/api/$token/lights/20/state |jq .
