#EXTM3U
#EXTINF:-1,NDR2
http://ndr-ndr2-niedersachsen.cast.addradio.de/ndr/ndr2/niedersachsen/mp3/128/stream.mp3

#EXTINF:-1,NDR 90.3 HH
http://ndr-ndr903-hamburg.cast.addradio.de/ndr/ndr903/hamburg/mp3/128/stream.mp3

#EXTINF:-1,NDR1 NDR1wellenord
http://ndr-ndr1wellenord-kiel.cast.addradio.de/ndr/ndr1wellenord/kiel/mp3/128/stream.mp3

#EXTINF:-1,NDR-Blue
http://ndr-ndrblue-live.cast.addradio.de/ndr/ndrblue/live/mp3/128/stream.mp3

#EXTINF:-1,N-JOY
http://ndr-njoy-live.cast.addradio.de/ndr/njoy/live/mp3/128/stream.mp3

#EXTINF:-1,R-SH Dance
http://streams.rsh.de/dance/mp3-192/mediaplayer

#EXTINF:-1,R-SH Livestream
http://streams.rsh.de/rsh-live/mp3-192/mediaplayer

#EXTINF:-1,R-SH Top 40
http://streams.rsh.de/rsh-nordparade/mp3-192/mediaplayer

#EXTINF:-1,R-SH 90er
http://streams.rsh.de/90er/mp3-192/mediaplayer

#EXTINF:-1,R-SH 80er
http://streams.rsh.de/rsh-80er/mp3-192/mediaplayer

#EXTINF:-1,R-SH Mittmann-Mix
http://streams.rsh.de/rsh-mittmannmix/mp3-192/mediaplayer

#EXTINF:-1,Rockantenne
http://mp3channels.webradio.rockantenne.de/rockantenne

#EXTINF:-1,Rockantenne Alternative
http://mp3channels.webradio.rockantenne.de/alternative

#EXTINF:-1,Rockantenne Heavy metal
http://mp3channels.webradio.rockantenne.de/heavy-metal

#EXTINF:-1,Rockantenne Classic Perlen
http://mp3channels.webradio.rockantenne.de/classic-perlen

#EXTINF:-1,Rockantenne Softrock
http://mp3channels.webradio.rockantenne.de/soft-rock

#EXTINF:-1,Rockantenne Young Stars
http://mp3channels.webradio.rockantenne.de/young-stars

#EXTINF:-1,Rockantenne Deutschrock
http://mp3channels.webradio.rockantenne.de/deutschrock

#EXTINF:-1,Rockantenne_HH
https://mp3channels.rockantenne.hamburg/rockantenne-hamburg

EXTINF:-1,Radio Hamburg
http://radiohamburg.hoerradar.de/radiohamburg-live-mp3-128?sABC=5po22397%230%23n830o3861r206q1q000no24qp1onq362%23yvaxenqvbunzohetqr&amsparams=playerid:linkradiohamburgde;skey:1555178391

EXTINF:-1,NORA Webstream
http://regiocast.hoerradar.de/nora-101-mp3-hq?sABC=57780r26%230%239n1r5qop20q7qop18sp7s9p2s844394o%23fgernzf.abenjrofgernzf.qr&amsparams=playerid:streams.norawebstreams.de;skey:1467485734

EXTINF:-1,NORA 80er
http://regiocast.hoerradar.de/nora-102-mp3-hq?sABC=57780r8n%230%239n1r5qop20q7qop18sp7s9p2s844394o%23fgernzf.abenjrofgernzf.qr&amsparams=playerid:streams.norawebstreams.de;skey:1467485834

EXTINF:-1,NORA Oldies
http://regiocast.hoerradar.de/nora-103-mp3-hq?sABC=57780rr9%230%239n1r5qop20q7qop18sp7s9p2s844394o%23fgernzf.abenjrofgernzf.qr&amsparams=playerid:streams.norawebstreams.de;skey:1467485929

EXTINF:-1,R-SA Partywelle
http://streams.rsa-sachsen.de/rsa-partywelle/mp3-192/streamurlsde/

EXTINF:-1,Schlagerradio
http://schlager-high.rautemusik.fm

EXTINF:-1,Radio FFH
http://mp3.ffh.de/radioffh/hqlivestream.mp3
